#############
Change Log
#############

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

0.5.0
************
MAP-291: NoiseDiode functionality refactored so it can be safely manipulated by an ENGINEER during OPERATE mode.

MAP-221: Attenuation has been refactored into attenuator 1 and 2 for more granular control of attenuation
         (before the software was only manipulating one of the two available)
         - noiseDiode manipulation is now allowed in any state
         - sync is now allowed in MAINTENANCE
         - monitorPing is now allowed in STARTUP

MAP-295: Refactor b3/4/5a/5bCapabilityState to report UNAVAILABLE instead of INVALID for DishLMC attribute rollup

0.4.1
************
MAP-225: Modify dishType to represent which dish the unit is installed into
         - Remove interfaceType property
         
MDSI-5: Removed ODL3 ADC accesses from the SPFRx for futureproofing

MAP-147: Fix operationalTime for all bands
         - Add operationalTime for RXPU
         - Add totOperationalTime for RXPU and all bands

0.4.0
************
MAP-226: Add control fiber and data fiber checks
         - controller/capturingData and controller/controlFiberCheck

MAP-217: Refactor and reenable ppsDeviation
         - ppsDeviation will now work on all WR units
         - Checks for bounce and alignment loss
         
0.3.10
************
MAP-190: Updating the following attributes so they work correctly:
         - samplingClockFrequency
         - b123ProcCntlSpectralInversion/WrAlignStatus/WrFifoOvf
         - bxSamplingClockFrequency
         - dataStreamBandChangeStatus/CurrentBand
         Removed the following attributes:
         - b123ProcCntlPpsdPolarity/SoftwareReset/ParamBand
         - datStreamCntlSoftwareReset
         Invalidated all of the following attributes until a decision can
         be made about the future of band 4/5:
         - b4* attributes, b5* attributes, b45* attributes, rxs45* attributes

0.3.9
************
MAP-216: Hotfix PPS Deviation so WR doesn't crash band-processor-123-ds @ITF

0.3.8
************
* MAP-190: PPS Deviation fixed, some attributes that were being read inappropriately
           now have state machines associated to them to make the logs readable
* MAP-199: DATA_CAPTURE operating state label has been removed where it still lingers
           and replaced with OPERATE
           
0.3.7
************
* MAP-119: Setting ODL3/SATRM k-value

0.3.6
************
* Set all release values correctly so the most recent binaries are deployed

0.3.5
************
* Adding sysid to spfrx_boardmap.json so the controller can initialize on startup

0.3.4
************
* MAP-132: swVersions reports sysid and controller versions now, the array has been refactored to
           an appropriate size, and the output has been refactored for readability
* Lint fix for local deployment with the Makefile

0.3.3
************
* MAP-129: Addressing intermittent FEC error on SPFRx startup

0.3.2
************
* MAP-123: Refactored the controller-ds so that configuredBand attribute is set at the right time

