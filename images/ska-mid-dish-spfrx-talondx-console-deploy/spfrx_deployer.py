#!/usr/bin/env python3
import argparse
import copy
import getpass
import json
import logging
import os
import re
import tarfile
import zipfile
from collections import OrderedDict

import requests
import tango
from conan_local.conan_wrapper import ConanWrapper
from nrcdbpopulate.dbPopulate import DbPopulate
from spfrx_config.spfrx_config import TalonDxConfig

LOG_FORMAT = "[spfrx_deployer.py: line %(lineno)s]%(levelname)s: %(message)s"


class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    OK = "\x1b[6;30;42m"
    FAIL = "\x1b[0;30;41m"
    ENDC = "\x1b[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


class Version:
    """
    Class to facilitate extracting and comparing version numbers in filenames.

    :param filename: string containing a version substring in the x.y.z
                     format, where x,y,z are numbers.
    """

    def __init__(self, filename):
        [ver_x, ver_y, ver_z] = re.findall("[0-9]+", filename)
        self.X = int(ver_x)
        self.Y = int(ver_y)
        self.Z = int(ver_z)

    def match(self, ver):
        """
        Compare two Version object and return true if the versions match.

        :param ver: Version object being compared to this one.
        """
        return self.X == ver.X and self.Y == ver.Y and self.Z == ver.Z


# POWER_SWITCH_USER = os.environ.get("POWER_SWITCH_USER")
# POWER_SWITCH_PASS = os.environ.get("POWER_SWITCH_PASS")


INTERNAL_BASE_DIR = "/app/images"
OCI_IMAGE_NAME = "ska-mid-dish-spfrx-talondx-console-deploy"
CONFIG_FILE = "spfrx-config-ska001.json"

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
ARTIFACTS_DIR = os.path.join(PROJECT_DIR, "artifacts")
DOWNLOAD_CHUNK_BYTES = 1024

TALONDX_STATUS_OUTPUT_DIR = os.environ.get("TALONDX_STATUS_OUTPUT_DIR")

GITLAB_PROJECTS_URL = "https://gitlab.drao.nrc.ca/api/v4/projects/"
GITLAB_API_HEADER = {
    "PRIVATE-TOKEN": f'{os.environ.get("GIT_ARTIFACTS_TOKEN")}'
}

NEXUS_API_URL = "https://artefact.skao.int/service/rest/v1/"
RAW_REPO_USER = os.environ.get("RAW_USER_ACCOUNT")
RAW_REPO_PASS = os.environ.get("RAW_USER_PASS")


def generate_spfrx_config(
    dish_id: str = "ska001",
    spfrx_instance: str = "spfrx-20",
) -> None:
    """
    Generates the config JSON file for the SPFRx

    :param dish_id: The dish id for which to generate the file
    """
    with open(
        f"{INTERNAL_BASE_DIR}/{OCI_IMAGE_NAME}/spfrx_config/spfrx_boardmap.json",
        "r",
    ) as config_map:
        config_map_str = config_map.read().replace("<dish-id>", dish_id)
        config_map_str = config_map_str.replace(
            "<spfrx-instance>", spfrx_instance
        )

        config_map_json = json.loads(config_map_str)

        spfrx_config_dict = {
            "ds_binaries": config_map_json["ds_binaries"],
            "fpga_bitstreams": config_map_json["fpga_bitstreams"],
        }

        db_servers_list = []
        for db_server in config_map_json["tango-db"]["db_servers"]:
            db_server_tmp = copy.deepcopy(db_server)
            db_servers_list.append(db_server_tmp)

        spfrx_config_dict["tango_db"] = {"db_servers": db_servers_list}
        spfrx_config_file = open(
            f"{INTERNAL_BASE_DIR}/{OCI_IMAGE_NAME}/artifacts/spfrx-config-{dish_id}.json",
            "w",
        )
        json.dump(spfrx_config_dict, spfrx_config_file, indent=6)
        spfrx_config_file.close()


def configure_db(inputjson) -> None:
    """
    Helper function for configuring DB entries using the dbpopulate module.
    """

    for server in inputjson:
        # TODO: make schema validation part of the dbPopulate class
        # with open( "./schema/dbpopulate_schema.json", 'r' ) as sch:
        with open("nrcdbpopulate/schema/dbpopulate_schema.json", "r") as sch:
            # schemajson = json.load(sch, object_pairs_hook=OrderedDict)
            json.load(sch, object_pairs_hook=OrderedDict)
            sch.seek(0)

        # try:
        #     logger_.info( "Validation step")
        #     jsonschema.validate( server, schemajson )
        # except ValidationError as error:
        #     handleValidationError( error, server )
        #     exit(1)

        dbpop = DbPopulate(server)

        # Remove and add to ensure any previous record is overwritten
        dbpop.process(mode="remove")
        dbpop.process(mode="add")


def configure_tango_db(tango_db):
    """
    Configure the Tango DB with devices specified in the talon-config
    JSON file.

    :param tango_db: JSON string containing the device server
                     specifications for populating the Tango DB
    """
    logger_.info("Configure Tango DB")
    configure_db(inputjson=tango_db.get("db_servers", ""))


def download_git_artifacts(git_api_url, name):
    response = requests.head(url=git_api_url, headers=GITLAB_API_HEADER)

    if response.status_code == requests.codes.ok:  # pylint: disable=no-member
        total_bytes = int(response.headers["Content-Length"])

        response = requests.get(
            git_api_url, headers=GITLAB_API_HEADER, stream=True
        )

        ds_artifacts_dir = os.path.join(ARTIFACTS_DIR, name)
        filename = os.path.join(ds_artifacts_dir, "artifacts.zip")
        bytes_downloaded = 0
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "wb") as fd:
            for chunk in response.iter_content(
                chunk_size=DOWNLOAD_CHUNK_BYTES
            ):
                fd.write(chunk)
                bytes_downloaded = min(
                    bytes_downloaded + DOWNLOAD_CHUNK_BYTES, total_bytes
                )
                per_cent = round(bytes_downloaded / total_bytes * 100.0)
                logger_.debug(
                    f"Downloading {total_bytes} bytes to "
                    f"{os.path.relpath(filename, PROJECT_DIR)} "
                    f"[{bcolors.OK}{per_cent:>3} %{bcolors.ENDC}]",
                    end="\r",
                )
            logger_.info("")

        logger_.info("Extracting files... ", end="")
        with zipfile.ZipFile(filename, "r") as zip_ref:
            zip_ref.extractall(ds_artifacts_dir)
        logger_.info(f"{bcolors.OK}done{bcolors.ENDC}")
    else:
        logger_.info(
            f"{bcolors.FAIL}status: {response.status_code}{bcolors.ENDC}"
        )


def download_raw_artifacts(api_url, name, filename):
    # TODO: factorize common code with download_git_artifacts
    response = requests.head(url=api_url, auth=(RAW_REPO_USER, RAW_REPO_PASS))

    if response.status_code == requests.codes.ok:  # pylint: disable=no-member
        total_bytes = int(response.headers["Content-Length"])

        response = requests.get(
            api_url, auth=(RAW_REPO_USER, RAW_REPO_PASS), stream=True
        )

        artifacts_dir = os.path.join(ARTIFACTS_DIR, name)
        filename = os.path.join(artifacts_dir, filename)
        bytes_downloaded = 0
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "wb") as fd:
            logger_.debug(
                f"Downloading {total_bytes} bytes to {os.path.relpath(filename, PROJECT_DIR)}"
            )
            for chunk in response.iter_content(
                chunk_size=DOWNLOAD_CHUNK_BYTES
            ):
                fd.write(chunk)
                bytes_downloaded = min(
                    bytes_downloaded + DOWNLOAD_CHUNK_BYTES, total_bytes
                )
                per_cent = round(bytes_downloaded / total_bytes * 100.0)
                print(
                    f"Downloading {total_bytes} bytes to {os.path.relpath(filename, PROJECT_DIR)} "
                    f"[{bcolors.OK}{per_cent:>3} %{bcolors.ENDC}]",
                    end="\r",
                )
            print("\n")

        logger_.info("Extracting files... ")
        if tarfile.is_tarfile(filename):
            tar = tarfile.open(filename, "r")
            tar.extractall(artifacts_dir)
            tar.close()
        else:
            with zipfile.ZipFile(filename, "r") as zip_ref:
                zip_ref.extractall(artifacts_dir)
        logger_.info(f"{bcolors.OK}Done extracting files{bcolors.ENDC}")

        # TODO: workaround; request permissions change from systems team
        os.chmod(os.path.join(artifacts_dir, "MANIFEST.skao.int"), 0o644)
    else:
        logger_.info(
            f"{bcolors.FAIL}File extraction failed - status: {response.status_code}{bcolors.ENDC}"
        )


def download_fpga_bitstreams(fpga_bitstreams):
    """
    Downloads and extracts FPGA bitstreams from the CAR (Common Artefact Repository),
    or Git pipeline artifacts.

    :param fpga_bitstreams: JSON string specifying which FPGA bitstreams to download.
    """
    for fpga in fpga_bitstreams:
        if fpga.get("source") == "raw":
            # Download the bitstream from the raw repo in CAR
            raw_info = fpga.get("raw")
            logger_.info(
                f"FPGA bitstream {raw_info['base_filename']}-{fpga['version']}"
            )

            fpga_bitstream_file = (
                f"{raw_info['base_filename']}-{fpga['version']}.tar.gz"
            )
            download_url = f"https://artefact.skao.int/repository/raw-internal/{fpga_bitstream_file}"
            download_raw_artifacts(
                api_url=download_url,
                name="fpga-talon",
                filename=fpga_bitstream_file,
            )
        else:
            # Download the artifacts from the latest successful Git pipeline
            git_info = fpga["git"]
            url = (
                f'{GITLAB_PROJECTS_URL}{git_info["git_project_id"]}/jobs/artifacts/'
                f'{git_info["git_branch"]}/download?job={git_info["git_pipeline_job"]}'
            )
            logger_.info(f"GitLab API call for bitstream download: {url}")

            # Alternate URL for downloading specific pipeline job
            """
            url = f"{GITLAB_PROJECTS_URL}{git_info['git_project_id']}/jobs/{git_info['git_pipeline_job']}/artifacts"
            logger_.info(f"GitLab API call for bitstream download: {url}")
            # https://gitlab.drao.nrc.ca/SKA/Mid.CBF/FW/persona/tdc_vcc_processing/-/jobs/12180
            """
            download_git_artifacts(
                git_api_url=url, name=f"fpga-{fpga['target']}"
            )

    # Modify the permissions of Artifacts dir so they can be
    # modified/deleted later
    chmod_r_cmd = "chmod -R o=rwx " + ARTIFACTS_DIR
    os.system(chmod_r_cmd)


def download_ds_binaries(ds_binaries, clear_conan_cache=True):
    """
    Downloads and extracts Tango device server (DS) binaries from
    Conan packages or Git pipeline artifacts.

    :param ds_binaries: JSON string specifying which DS binaries to download.
    :param clear_conan_cache: if true, Conan packages are fetched from
                              remote; default true.
    """
    conan = ConanWrapper(ARTIFACTS_DIR)
    logger_.info(f"Conan version: {conan.version()}")
    if clear_conan_cache:
        logger_.info(f"Conan local cache: {conan.search_local_cache()}")
        logger_.info(
            f"Clearing Conan local cache... {conan.clear_local_cache()}"
        )
    logger_.info(f"Conan local cache: {conan.search_local_cache()}")

    for ds in ds_binaries:
        logger_.info(f"DS Binary: {ds['name']}")

        if ds.get("source") == "conan":
            # Download the specified Conan package
            conan_info = ds.get("conan")
            logger_.info(f"Conan info: {conan_info}")
            conan.download_package(
                pkg_name=conan_info["package_name"],
                version=conan_info["version"],
                user=conan_info["user"],
                channel=conan_info["channel"],
                profile=os.path.join(
                    conan.profiles_dir, conan_info["profile"]
                ),
            )
        elif ds.get("source") == "git":
            # Download the artifacts from the latest successful pipeline
            git_info = ds.get("git")
            url = (
                f'{GITLAB_PROJECTS_URL}{git_info["git_project_id"]}'
                f"/jobs/artifacts/"
                f'{git_info["git_branch"]}/download?job='
                f'{git_info["git_pipeline_job"]}'
            )
            download_git_artifacts(git_api_url=url, name=ds["name"])
        else:
            logger_.info(f'Error: unrecognized source ({ds.get("source")})')
            exit(-1)

    # Modify the permissions of Artifacts dir so they can be
    # modified/deleted later
    chmod_r_cmd = "chmod -R o=rwx " + ARTIFACTS_DIR
    os.system(chmod_r_cmd)


if __name__ == "__main__":
    logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
    logger_ = logging.getLogger("spfrx_deployer.py")
    logger_.info(f"User: {getpass.getuser()}")
    parser = argparse.ArgumentParser(
        description="MID DISH SPFRx Talon-DX Deployer Utility"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="increase output verbosity",
        action="store_true",
    )
    parser.add_argument(
        "--config-db",
        help="configure the Tango database with devices specified",
        action="store_true",
    )
    parser.add_argument(
        "--generate-spfrx-config",
        help="Generate SPFRx config file",
        action="store_true",
    )
    parser.add_argument(
        "--download-artifacts",
        help="download the Tango DS binaries from the SKA CAR",
        action="store_true",
    )
    parser.add_argument(
        "--dish_id",
        type=str,
        metavar="DISH_ID",
        default="ska001",
        help="Specify the dish id",
    )
    parser.add_argument(
        "--spfrx_instance",
        type=str,
        metavar="SPFRX_INSTANCE",
        default="spfrx-20",
        help="Specify the spfrx tango instance",
    )
    args = parser.parse_args()

    if args.config_db:
        spfrx_config_file = os.path.join(
            ARTIFACTS_DIR, f"spfrx-config-{args.dish_id}.json"
        )
        config = TalonDxConfig(config_file=spfrx_config_file)
        logger_.info(
            f"Configure DB - TANGO_HOST = "
            f"{tango.ApiUtil.get_env_var('TANGO_HOST')} "
            f"using config {spfrx_config_file}"
        )
        configure_tango_db(config.tango_db())
    elif args.download_artifacts:
        spfrx_config_file = os.path.join(
            ARTIFACTS_DIR, f"spfrx-config-{args.dish_id}.json"
        )
        logger_.info(f"Download Artifacts using config {spfrx_config_file}")
        config = TalonDxConfig(config_file=spfrx_config_file)
        config.export_config(ARTIFACTS_DIR)
        download_ds_binaries(config.ds_binaries())
        download_fpga_bitstreams(config.fpga_bitstreams())
    elif args.generate_spfrx_config:
        logger_.info(
            f"Generate SPFRx Configuration JSON file spfrx-config-{args.dish_id}.json"
        )
        generate_spfrx_config(args.dish_id, args.spfrx_instance)
    else:
        logger_.info("Hello from Mid DISH SPFRx Deployer")
